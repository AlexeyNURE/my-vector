#ifndef _VECTOR_HPP_
#define _VECTOR_HPP_
#include <iostream>
#include <type_traits>
#include <cassert>
#include <utility>

template <typename T>
struct Vector
{
private:
	void * m_data;
	int m_nUsed;
	int m_nAllocated;
	void grow();
	const T* data() const;
	T* data();
	void copy(const Vector &);
public:
	Vector();
	Vector(int _allocated );
	~Vector();
	Vector(const Vector &);
	Vector & operator = (const Vector &);
	Vector(const Vector &&);
	Vector & operator = (const Vector &&);
	bool operator == (const Vector &) const;
	bool operator != (const Vector &) const;
	int size()  const;
	int capacity() const;
	T& back();
	T& front();
	const T& back() const;
	const T& front() const;
	T & operator [] (int _index);
	const T & operator [] (int _index) const;
	void clear();
	bool empty() const;
	void push_back(const T & _argsB);
	void pop_back();
	void shrink_to_fit();
	void insert_at(int _pos, const T &);
	T &at(int _index);
	const T &at(int _index) const;
	void erase_at(int _pos);
	void print() const;
	void emplace_back();
	void reserve(int);
	void assign(size_t _amount, const T &);

	template <class MyVector>
	class iterator
	{

	private:

		size_t m_currentPosition;

		MyVector & m_vector;

	public:

		iterator(MyVector & _vector, unsigned int _currentPositon);

		size_t getCurrentPosition() const;

		bool operator == (iterator _it) const;

		bool operator != (iterator _it) const;

		bool operator >= (iterator _it) const;

		bool operator <= (iterator _it) const;

		bool operator > (iterator _it) const;

		bool operator < (iterator _it) const;

		iterator<MyVector> operator + (int _step) const;

		iterator<MyVector> operator - (int _step) const;

		iterator<MyVector> & operator -= (int _step);

		iterator<MyVector> & operator += (int _step);

		iterator<MyVector> & operator + (iterator _it);

		iterator<MyVector> & operator - (iterator _it);

		iterator<MyVector> & operator = (iterator _it);

		iterator<MyVector> & operator -= (iterator _it);

		iterator<MyVector> & operator += (iterator _it);

		iterator<MyVector> & operator ++ ();

		iterator<MyVector> & operator ++ (int);

		iterator<MyVector> & operator -- ();

		iterator<MyVector> & operator -- (int);

		const T & operator *() const;

		template< typename = std::enable_if< !(std::is_const< MyVector >::value) >::type >
		T & operator*();

		
	};

	using Iterator = typename Vector<T>::iterator<Vector<T>>;
	using const_Iterator = typename Vector<T>::iterator< const Vector<T>>;

	Iterator begin();
	Iterator end();
	const_Iterator cbegin() const;
	const_Iterator cend() const;

};


template <typename T>
Vector<T>::Vector()
	:m_nUsed(0), m_nAllocated(16)
{
	m_data = operator new (sizeof(T) * m_nAllocated);
}

template <typename T>
Vector<T>::Vector(int _alloc)
	:m_nUsed(0), m_nAllocated(_alloc)
{
	m_data = operator new (sizeof(T) * m_nAllocated);
}

template <typename T>
void Vector<T>::copy(const Vector &_vector)
{
	int size = _vector.size();
	for (int i(0); i < size; i++)
		push_back(_vector[i]);
}


template<typename T>
Vector<T>::Vector(const Vector &_vector)
	:Vector()
{
	copy(_vector);
}

template<typename T>
Vector<T> & Vector<T>::operator=(const Vector &_vec)
{
	if (_vec == *this)
		return *this;
	clear();
	copy(_vec);
	return *this;
}

template<typename T>
 bool Vector<T>::operator==(const Vector &_vec) const
{
	 if (size() != _vec.size())
		 return false;

	 for (int i(0); i < m_nUsed; i++)
		 if (data()[i] != _vec[i])
			 return false;
	 return true;
}

template<typename T>
bool Vector<T>::operator!=(const Vector &_vec) const
{
	return !(*this == _vec);
}

template <typename T>
Vector<T>::~Vector()
{
	clear();
	operator delete (m_data);
}

template <typename T>
void Vector<T>::insert_at(int _pos, const T & _val)
{
	assert(_pos >= 0 && _pos <= m_nUsed);
	if (m_nUsed + 1 > m_nAllocated)
		grow();

	for (int i(m_nUsed); i > _pos; i--)
		new (data() + i) T (*(data() + i - 1));

	new (data() + _pos) T(_val);
	m_nUsed++;
}

template <typename T>
void Vector<T>::erase_at(int _pos)
{
	assert(_pos >= 0 && _pos <= m_nUsed);

	data()[_pos].~T();

	for (int i(_pos + 1); i < m_nUsed; i++)
		new (data() + i - 1) T (*(data() + i));

	m_nUsed--;
}

template <typename T>
const T & Vector<T>::at(int _index) const
{
	assert(_index >= 0 && _index <= m_nUsed);
	return data()[_index];
}

template <typename T>
T & Vector<T>::at(int _index)
{
	assert(_index >= 0 && _index <= m_nUsed);
	return data()[_index];
}

template <typename T>
int Vector<T>::size() const
{
	return m_nUsed;
}

template <typename T>
void Vector<T>::clear()
{
	while (size())
		pop_back();
}

template <typename T>
T* Vector<T>::data()
{
	return static_cast<T*> (m_data);
}

template <typename T>
const T *Vector<T>::data() const
{
	return static_cast<T*> (m_data);
}

template <typename T>
void Vector<T>::grow()
{
	int nAllocatedNew = m_nAllocated * 2;
	void * pNewData = operator new (sizeof(T)* nAllocatedNew);

	memcpy(pNewData, m_data, sizeof(T) * m_nAllocated);

	operator delete(m_data);

	m_data = pNewData;

	m_nAllocated = nAllocatedNew;
}

template <typename T>
void Vector<T>::push_back(const T & _args)
{
	if (m_nUsed == m_nAllocated)
		grow();

	 new (data() + m_nUsed++) T ( _args );
}

template <typename T>
T & Vector<T>::operator [] (int _index)
{
	return data()[_index];
}

template <typename T>
const T & Vector<T>::operator [] (int _index) const
{
	return data()[_index];
}

template <typename T>
 const T& Vector<T>::back() const
{
	 assert(!empty());
	 *(data() + m_nUsed - 1);
}

 template <typename T>
  const T& Vector<T>::front() const
 {
	 assert(!empty());
	 return *data();
 }

  template <typename T>
  T & Vector<T>::front()
  {
	  assert(!empty());
	  return *data();
  }

  template <typename T>
  T & Vector<T>::back()
  {
	  assert(!empty());
	  return *(data() + m_nUsed - 1);
  }

template <typename T>
void Vector<T>::pop_back()
{
	if (!empty())
	{
		if (std::is_class<T>::value)
			data()[m_nUsed-1].~T();

		m_nUsed--;
	}
	else throw std::logic_error("Empty vector");
}

template <typename T>
bool Vector<T>::empty() const
{
	return m_nUsed == 0;
}

template <typename T>
int Vector<T>::capacity() const
{
	return m_nAllocated;
}

template <typename T>
void Vector<T>::print() const
{
	for (int i(0); i < m_nUsed; i++)
		std::cout << data()[i] << ' ';
}

template <typename T>
void Vector<T>::shrink_to_fit()
{
	int nAllocatedNew = m_nUsed;
	void * pNewData = operator new (sizeof(T)* nAllocatedNew);

	memcpy(pNewData, m_data, sizeof(T) * m_nUsed);

	operator delete(m_data);

	m_data = pNewData;

	m_nAllocated = nAllocatedNew;
}

template <typename T>
void Vector<T>::reserve(int _size)
{
	int nAllocatedNew = m_nAllocated;

	while (_size > nAllocatedNew)
		nAllocatedNew *= 2;

	void * pNewData = operator new (sizeof(T)* nAllocatedNew);

	memcpy(pNewData, m_data, sizeof(T) * m_nAllocated);

	operator delete(m_data);

	m_data = pNewData;

	m_nAllocated = nAllocatedNew;
}

template <typename T>
void Vector<T>::assign(size_t _amount, const T & _value)
{
	for (int i(0); i < _amount; i++)
		push_back(_value);
}

template<typename T>
template<class MyVector>
Vector<T>::iterator<MyVector>::iterator(MyVector & _vector, unsigned int _currentPositon)
	:m_vector(_vector), m_currentPosition(_currentPositon)
{
}

template<typename T>
template<class MyVector>
inline size_t Vector<T>::iterator<MyVector>::getCurrentPosition() const
{
	return m_currentPosition;
}

template <typename T>
template<class MyVector>
bool Vector<T>::iterator<MyVector>::operator ==(iterator _it) const
{
	return m_currentPosition == _it.m_currentPosition;
}

template <typename T>
template<class MyVector>
bool Vector<T>::iterator<MyVector>::operator != (iterator _it) const
{
	return m_currentPosition != _it.m_currentPosition;
}

template<typename T>
template<class MyVector>
bool Vector<T>::iterator<MyVector>::operator>=(iterator _it) const
{
	return m_currentPosition >= _it.m_currentPosition;
}

template<typename T>
template<class MyVector>
bool Vector<T>::iterator<MyVector>::operator<=(iterator _it) const
{
	return m_currentPosition <= _it.m_currentPosition;
}

template<typename T>
template<class MyVector>
bool Vector<T>::iterator<MyVector>::operator>(iterator _it) const
{
	return m_currentPosition > _it.m_currentPosition;
}

template<typename T>
template<class MyVector>
bool Vector<T>::iterator<MyVector>::operator<(iterator _it) const
{
	return m_currentPosition < _it.m_currentPosition;
}

template<typename T>
template<class MyVector>
 Vector<T>::iterator<MyVector> Vector<T>::iterator<MyVector>::operator+(int _step) const
{
	assert(m_currentPosition + _step < m_vector.size());
	Vector<T>::iterator<MyVector> res(m_vector, m_currentPosition);
	res.m_currentPosition += _step;
	return res;
}


template<typename T>
template<class MyVector>
 Vector<T>::iterator<MyVector> Vector<T>::iterator<MyVector>::operator-(int _step) const
{
	assert(m_currentPosition - _step >= 0);
	Vector<T>::iterator<MyVector> res(m_vector, m_currentPosition);
	res.m_currentPosition -= _step;
	return res;
}

template<typename T>
template <class MyVector>
Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator-=(int _step) 
{
	assert(m_currentPosition - _step >= 0);
	m_currentPosition -= _step;
	return *this;
}

 template<typename T>
 template<class MyVector>
  Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator+=(int _step) 
 {
	  assert(m_currentPosition + _step < m_vector.size());
	  m_currentPosition += _step;
	  return *this;
 }

template<typename T>
template<class MyVector>
   Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator+(iterator _it)
{
	   assert(m_currentPosition + _it.m_currentPosition < m_vector.size());
	   Vector<T>::iterator<MyVector> res = *this;
	   res.m_currentPosition += it.m_currentPosition;
	   return res;
}

template<typename T>
template<class MyVector>
 Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator-(iterator _it)
{
	 assert(m_currentPosition - _it.m_currentPosition >= 0);
	 Vector<T>::Iterator res = *this;
	 res.m_currentPosition -= it.m_currentPosition;
	 return res;
}

 template<typename T>
 template<class MyVector>
 Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator=(iterator _it)
 {
	 m_currentPosition = _it.m_currentPosition;
	 return *this;
 }

 template<typename T>
 template<class MyVector>
  Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator-=(iterator _it)
 {
	  assert(m_currentPosition - _it.m_currentPosition >= 0);
	  m_currentPosition -= _it.m_currentPosition;
	  return *this;
 }

  template<typename T>
  template<class MyVector>
  Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator+=(iterator _it)
{
	  assert(m_currentPosition + _it.m_currentPosition < m_vector.size());
	  m_currentPosition += _it.m_currentPosition;
	  return *this;
}

template<typename T>
template<class MyVector>
 Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator++()
{
	assert(m_currentPosition + 1 < m_vector.size());
	m_currentPosition++;
	return *this;
}

template<typename T>
template<class MyVector>
 Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator--()
{
	 assert(m_currentPosition - 1 >= 0);
		m_currentPosition--;
	return *this;
}

template<typename T>
template<class MyVector>
 Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator++(int)
{
	 assert(m_currentPosition + 1 < m_vector.size());
	 Vector<T>::iterator<MyVector> res = *this;
	 ++m_currentPosition;
	 return res;
 }

 template<typename T>
 template<class MyVector>
 Vector<T>::iterator<MyVector> & Vector<T>::iterator<MyVector>::operator--(int)
 {
	 assert(m_currentPosition - 1 >= 0);
	 Vector<T>::iterator<MyVector> res = *this;
	 --m_currentPosition;
	 return res;
 }

template<typename T>
template<class MyVector>
const T & Vector<T>::iterator<MyVector>::operator*()const 
{
	return m_vector[m_currentPosition];
}

template<typename T>
template<class MyVector>
template <typename = std::enable_if < !(std::is_const<MyVector>::value )> ::type >
T& Vector<T>::iterator<MyVector>::operator*()
{
	return m_vector[m_currentPosition];
}


template<typename T>
typename Vector<T>::Iterator  Vector<T>::begin()
{
	return Vector <T>::Iterator (*this, 0);
}

template<typename T>
typename Vector<T>::Iterator Vector<T>::end()
{
	return Vector <T>::Iterator (*this, m_nUsed);
}

template<typename T>
typename Vector<T>::const_Iterator Vector<T>::cbegin() const
{
	Vector<T>::const_Iterator it(*this, 0);
	return it;
}

template<typename  T>
typename Vector<T>::const_Iterator Vector<T>::cend() const
{
	Vector<T>::const_Iterator it(*this, m_nUsed);
	return it;
}

#endif
