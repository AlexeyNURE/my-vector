#include "testslib.hpp"
#include "Vector.hpp"
#include <vector>

DECLARE_OOP_TEST(test_simple_vec)
{
	Vector<int> v;
	assert(v.empty());
	assert(v.size() == 0);
	assert(v.capacity() == 16);

	v.push_back(1);
	assert(v.size() == 1);
	assert(v.back() == v.front() == 1);
	v.pop_back();
	assert(v.empty());
}

DECLARE_OOP_TEST(test_vector_push)
{
	Vector<double> v(10);
	v.push_back(1.0);
	v.push_back(2.0);
	v.push_back(3.0);
	assert(v.size() == 3);
	assert(v.capacity() == 10);
	assert(v.back() == 3.0);
	assert(v.front() == 1.0);
	assert(v[1] == 2.0);
	assert(v.at(2) == 3.0);
}

DECLARE_OOP_TEST(test_copy_constructors)
{
	Vector <std::string> a;
	a.push_back("one");
	a.push_back("two");
	a.push_back("three");
	Vector<std::string> copy(a);
	assert(a == copy);
	Vector<std::string> copy2;
	copy2.push_back("lol");
	copy2.push_back("kek");
	copy2.push_back("cheburek");
	copy2 = a;
	assert(copy2 == a);
	assert(copy2.front() == "one");
}

DECLARE_OOP_TEST(test_some_funcs)
{
	Vector<char> v(5);
	v.push_back('a');
	v.push_back('b');
	v.push_back('d');
	v.insert_at(2, 'c');
	assert(v.front() == 'a');
	assert(v[1] == 'b');
	assert(v[2] == 'c');
	assert(v.back() == 'd');
	assert(v.size() == 4);
	assert(!v.empty());	
	assert(v.capacity() == 5);

	v.insert_at(0, '@');
	v.push_back('e');
	assert(v.capacity() == 10);
	assert(v.size() == 6);
	v.insert_at(3, 'o');

	v.shrink_to_fit();


	assert(v.capacity() == 7);
	assert(v.size() == 7);
	v.erase_at(3);	

	assert(v.front() == '@');
	assert(v.at(3) == 'c');
	assert(v.back() == 'e');

	v.push_back('f');
	assert(v.capacity() == 7);
	assert(v.size() == 7);
	v.insert_at(7, 'g');
	assert(v.capacity() == 14);
	assert(v.back() == 'g');
}

DECLARE_OOP_TEST(test_vector_of_class)
{
	Vector<std::string> v;
	v.push_back("Hello");
	v.push_back("world!");
	v.push_back("My friend");
	assert(v.back() == "My friend");
	v.erase_at(0);
	assert(v.size() == 2);
	assert(v.front() == "world!");
	assert(v.back() == "My friend");

	v.clear();

	assert(v.empty());
	assert(v.capacity() == 16);
}

DECLARE_OOP_TEST(test_reserve)
{
	Vector<std::string> a;
	a.reserve(40);
	assert(a.empty());
	assert(a.capacity() == 64);

	a.push_back("kek");
	a.push_back("123");
	a.shrink_to_fit();
	assert(a.capacity() == 2);
	a.reserve(20);
	assert(a.capacity() == 32);
	assert(a.back() == "123");
	assert(a.front() == "kek");
}

DECLARE_OOP_TEST(test_iterator)
{
	Vector<std::string> a;
	a.push_back("zero");
	a.push_back("one");
	a.push_back("two");
	a.push_back("three");
	a.push_back("four");
	a.push_back("five");
	auto it = a.begin();
	assert(*it == "zero");
	it++;
	assert(*it == "one");
	++it;
	assert(*it == "two");
	it += 2;
	assert(*it == "four");
	it -= 2;
	assert(*it == "two");
	it--;
	assert(*it == "one");
	--it;
	assert(*it == "zero");
}

DECLARE_OOP_TEST(test_iterator_2)
{
	Vector<std::string> a;
	a.push_back("zero");
	a.push_back("one");
	a.push_back("two");
	a.push_back("three");
	a.push_back("four");
	a.push_back("five");
	auto itBegin = a.begin();
	auto it1 = a.begin() + 2;
	assert(*it1 == "two");
	itBegin++;
	itBegin += it1;
	assert(*itBegin == "three");
	it1 = itBegin;
	assert(*it1 == *itBegin);

	auto c1 = a.begin();
	assert(*c1 == "zero");
	*c1 = "lol";
	assert(*c1 == "lol");


	
	auto c2 = a.cbegin();
	assert(*c2 == "lol");
	//*c2 = "kek";
}

DECLARE_OOP_TEST(test_iterator_3)
{
	Vector<std::string> a;
	a.push_back("zero");
	a.push_back("one");
	a.push_back("two");
	a.push_back("three");
	a.push_back("four");
	a.push_back("five");
	auto it = a.cbegin();
	//*it = "lol";
	assert((*it == "zero"));
}


int main()
{
	gs_TestsRunner.runTests();
	return 0;
}
/*****************************************************************************/